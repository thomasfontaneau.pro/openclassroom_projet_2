package com.hemebiotech.analytics;

/**
 * Main class of project
 */
public class AnalyticsCounter {
    public static void main(String[] args) {
        ReadSymptomDataFromFile readSymptomDataFromFile = new ReadSymptomDataFromFile("openclassroom_projet_2/Project02Eclipse/symptoms.txt");
        readSymptomDataFromFile.readFile();
        readSymptomDataFromFile.treatmentData();
        readSymptomDataFromFile.generationResult();
    }
}

