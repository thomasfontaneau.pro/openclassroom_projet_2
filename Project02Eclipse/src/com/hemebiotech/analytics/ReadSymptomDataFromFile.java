package com.hemebiotech.analytics;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ReadSymptomDataFromFile implements ISymptomReader {

    private String filePath;
    List<String> listSymptoms = new ArrayList<>();
    Map<String, Integer> resultSymptoms = new LinkedHashMap<>();

    public ReadSymptomDataFromFile(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Read the file and add they name read's in the list.
     */
    @Override
    public void readFile() {
        try {
            FileInputStream file = new FileInputStream(filePath);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                listSymptoms.add(scanner.nextLine());
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Count the number of iterations of each word in the list and add the result in a map.
     */
    @Override
    public void treatmentData() {
        List<String> sortSymptoms = sortAlphabetical(listSymptoms);
        sortSymptoms.forEach(symptom -> {
            if (!resultSymptoms.containsKey(symptom)) {
                resultSymptoms.put(symptom, Collections.frequency(sortSymptoms, symptom));
            }
        });
    }

    /**
     * Write the map result in the file of out.
     */
    @Override
    public void generationResult() {
        try {
            File file = new File("openclassroom_projet_2/Project02Eclipse/results.out");
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            resultSymptoms.forEach((symptoms, nb) -> {
                try {
                    bw.write(symptoms + "=" + nb + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sort a list of string to the order alphabetical.
     * @param symptoms list of symptom.
     * @return list of symptom sort by order alphabetical.
     */
    @Override
    public List<String> sortAlphabetical(List<String> symptoms) {
        Collections.sort(symptoms);
        return symptoms;
    }

    /**
     * Allows you to get the file path.
     * @return the file path
     */
    public String getFilePath() { return filePath; }

    /**
     * Change the value of variable filePath.
     * @param filePath the file path
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
