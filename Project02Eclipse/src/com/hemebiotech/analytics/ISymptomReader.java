package com.hemebiotech.analytics;

import java.util.List;

/**
 * Interface
 */
public interface ISymptomReader {

    void readFile();
    void treatmentData();
    void generationResult();
    List<String> sortAlphabetical(List<String> symptoms);
}
