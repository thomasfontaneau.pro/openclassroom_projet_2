<h1 style="color: orange">Projet n°2</h1>
<h2>Debug a Java application</h2>

<p>The goal of the project is to create an output file containing the number of iterations of each symptom.</p>

<h3>Example input file :</h3>

```
dialated pupils
dry mouth
inflamation
tremor
stomach pain
high blood pressure
stiff neck
cough
insomnia
headache
insomnia
constricted pupils
nausea
cough
```

<h3>Example output file :</h3>

```
anxiety=5
arrhythmias=3
blindness=1
blurred vision=5
constricted pupils=3
cough=6
dialated pupils=4
dizziness=5
dry mouth=8
fever=7
headache=3
```

<h3>How to run the project</h3>

Clone the project on your local repository and run AnalyticsCounter.java.